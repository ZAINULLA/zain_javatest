package com.challenges.level100;

import java.util.*;
import java.io.*;

//import com.challenges.tutor.level100.*;
/**
*2).Given a list of names, make a sublist with elements which have more than 4 characters.
*   ['The', 'practice', 'has', 'been' ,'repeatedly' ,'endorsed' ,'by' ,'Congress']
* Run the command from the root directory of this project to validate if this Question is answered
* correctly
* './gradlew test -Dtest.single='*level100/Question2''
*/
public class Question2_Solution {
    /**
    *Note:While returning the result as a map, in key:value pair always use key as "subList"
    * A sample return Map look like this:
    * {"subList":["Congress","practice",..]}
    */
   public static Map solve() { 
		Scanner scan=new Scanner(System.in);
		int n=0;
		System.out.println("ENTER NO OF WORDS");
		n=scan.nextInt();
		scan.nextLine();
		String s[];
		s=new String[n];
		for(int i=1;i<=n;i++)
		{
		System.out.println("ENTER "+i+" WORD");
		s[i-1]=scan.nextLine();
		}
			
		Map map=new HashMap();  
		//Adding elements to map  
		for(int i=0;i<s.length;i++)
		{
			if(s[i].length()>3)
			{
				
			    map.put(i,s[i]);
		//this adds an element to the list.
			}
	
		}
//Traversing Map  
Set set=map.entrySet();//Converting to Set so that we can traverse  
Iterator itr=set.iterator();  
while(itr.hasNext()){  
    //Converting to Map.Entry so that we can get key and value separately  
Map.Entry entry=(Map.Entry)itr.next();  
System.out.println("sublist:"+entry.getKey()+" "+entry.getValue());  
    }
	return map; 
	
} 
public static void main(String[] args)
{
	solve();
}
}  